// jQuery
// declare var jQuery: any;
// // //Require
// declare var require: any;

// window['jQuery'] = require('jquery');
// window['$'] = window['jQuery'];

declare var System: SystemJS;

interface SystemJS {
    import: (path?: string) => Promise<any>;
}

