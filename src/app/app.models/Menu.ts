export class Menu {

    menuId: number;
    menuDescripcion: string;
    menuCodigo: string;
    menuLink: string;
    menuPadre: number;
    menuHijos: boolean;
    menuIcon: string;
    menuRoles: string;

    constructor(id: number, descripcion: string, codigo: string, link: string, icon: string, roles: string, padre: number = 0, tieneHijos: boolean = false) {
        this.menuId = id;
        this.menuDescripcion = descripcion;
        this.menuCodigo = codigo;
        this.menuLink = link;
        this.menuPadre = padre;
        this.menuHijos = tieneHijos;
        this.menuIcon = icon;
        this.menuRoles = roles;
    }

    canShow(rolesStr) {
        let rolAb = rolesStr.split(',').filter(rol => {
            return this.menuRoles.indexOf(rol) !== -1;
        });
        return rolAb && rolAb.length > 0;
    }

}