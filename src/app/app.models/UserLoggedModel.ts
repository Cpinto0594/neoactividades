export class UserLoggedModel {
    userName: string;
    userId: Number;
    userToken: string;
    RfTkn:string;
    roles: string;
    userFullName: string;
    userPic: string;
    lastLoggedDate: string;
    userEmail: string;
    userIdentificacion: string;
    userProyectos: Array<any> = [];
    userEmpresas: Array<any> = [];

    constructor() {

    }

}