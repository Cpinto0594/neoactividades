import { Component, OnInit } from '@angular/core';
import { NotificationServices } from 'app/app.services/NotificationService';
import { ActividadesServices } from './actividades.services';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { Util } from 'app/app.services/Util.services';
import { AuthService } from 'app/app.services/AuthService';
import { LoadingService } from 'app/app.services/LoadingService';
import { sha256 } from 'js-sha256';
import { EmpresasServices } from '../empresas.module/empresas.services';
import { ProyectosServices } from '../proyectos.module/proyectos.services';
import { of } from 'rxjs/observable/of';

declare var $: any;

@Component({
    selector: 'datos_personales-form',
    templateUrl: './datos_personales.component.html'
})
export class DatosPersonalesComponent implements OnInit {

    public userData: any;
    private lastPassword: string;
    public empresasList: Array<any>;
    public proyectosList: Array<any>;
    public empresaSeleccionadas: Array<any>;
    public proyectosSeleccionados: Array<any>;
    public empresaSeleccionada: any;
    public proyectoSeleccionada: any;

    constructor(private notificaciones: NotificationServices,
        private userServices: UsuariosServices,
        private utilities: Util,
        private authService: AuthService,
        private loadingService: LoadingService,
        private empresasServices: EmpresasServices,
        private proyectosServices: ProyectosServices,
        private usuariosService: UsuariosServices) {
    }

    ngOnInit() {
        this.userData = {};
        this.findUserData();
        this.findEmpresas();
        this.findProyectos();
    }

    findUserData() {
        this.loadingService.showLoading();

        of(this.authService.userLoggedData())
            .subscribe(data => {

                this.userData = {
                    identificacion: data.userIdentificacion,
                    nombre_completo: data.userFullName,
                    usuario: data.userName,
                    id: data.userId,
                    email: data.userEmail
                }
                this.userData.clave = undefined;

                this.empresaSeleccionadas = data.userEmpresas.map(empresa => { return { id: empresa.empresa_id, text: empresa.descripcion } });
                this.proyectosSeleccionados = data.userProyectos.map(proyecto => { return { id: proyecto.proyecto_id, text: proyecto.descripcion } });

            }, (err) => {
                this.notificaciones.error('No se pudo cargar Información ' + err.message);
                this.loadingService.closeLoading()
            },
                () => {
                    this.loadingService.closeLoading();
                });
    }

    findEmpresas() {
        this.empresasServices.getAllActive()
            .subscribe((data) => {
                this.empresasList = (data.data || [])
            })
    }
    findProyectos() {
        this.proyectosServices.getAllActive()
            .subscribe((data) => {
                this.proyectosList = (data.data || [])
            });
    }

    agregarProyecto() {
        if (!this.proyectoSeleccionada) return;
        let exist = this.proyectosSeleccionados
            .map(proyecto => +proyecto.id).indexOf(+this.proyectoSeleccionada.id) !== -1

        if (!exist)
            this.proyectosSeleccionados.push(this.proyectoSeleccionada);
        this.proyectoSeleccionada = null;

    }
    agregarEmpresa() {
        if (!this.empresaSeleccionada) return;
        let exist = this.empresaSeleccionadas
            .map(empresa => +empresa.id).indexOf(+this.empresaSeleccionada.id) !== -1

        if (!exist)
            this.empresaSeleccionadas.push(this.empresaSeleccionada);
    }

    guardarDatos() {
        if (!this.userData || !this.userData.usuario || !this.userData.nombre_completo
            || !this.userData.identificacion || !this.userData.email) {
            this.notificaciones.info('Debe Ingresar la información Requerida');
            return;
        }

        this.loadingService.showLoading();

        if (this.userData.clave) {
            this.userData.clave = sha256(this.userData.clave);
        }

        let data = {
            usuario: this.userData,
            empresas: this.empresaSeleccionadas.map(empresa => +empresa.id),
            proyectos: this.proyectosSeleccionados.map(proyecto => +proyecto.id)
        }


        this.userServices.createAndAsocciate(data)
            .subscribe(data => {
                if (data.success) {
                    this.notificaciones.success('Actualización exitosa ');

                    let usuario = this.authService.userLoggedData();
                    usuario.userEmail = this.userData.email;
                    usuario.userFullName = this.userData.nombre_completo;
                    usuario.userIdentificacion = this.userData.identificacion;

                    usuario.userEmpresas = this.empresaSeleccionadas.map(emp => ({ empresa_id: emp.id, descripcion: emp.text }));
                    usuario.userProyectos = this.proyectosSeleccionados.map(pro => ({ proeycto_id: pro.id, descripcion: pro.text }));
                    AuthService.setUserData(usuario);
                } else {
                    this.notificaciones.error('No se pudo actualizar la información');
                }
            }, (err) => {
                this.notificaciones.error('No se pudo cargar Información ' + err.message)
                this.loadingService.closeLoading();
            },
                () => {
                    this.loadingService.closeLoading();
                });
    }

    eliminarproyecto(proyecto) {
        if (!proyecto || !this.userData || !this.userData.id) {
            this.notificaciones.error('Debe Seleccionar Datos para la aliminación');
            return;
        }

        this.loadingService.showLoading();

        let data = {
            proyecto: proyecto
        }
        this.usuariosService.desAsociarProyectos(this.userData.id, data)
            .subscribe(response => {
                if (response.success) {

                    this.proyectosSeleccionados.forEach((item, index) => {
                        if (+item.id === +proyecto) {
                            this.proyectosSeleccionados.splice(index, 1);
                        }
                    });

                } else {
                    this.notificaciones.error('No se pudo realizar el proceso ' + response.message);
                }
            }, () => {
                this.notificaciones.error('No se pudo realizar el proceso');
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            })

    }

    eliminarEmpresa(empresa) {
        if (!empresa || !this.userData || !this.userData.id) {
            this.notificaciones.error('Debe Seleccionar Datos para la aliminación');
            return;
        }

        this.loadingService.showLoading();

        let data = {
            empresa: empresa
        }
        this.usuariosService.desAsociarEmpresas(this.userData.id, data)
            .subscribe(response => {
                if (response.success) {

                    this.empresaSeleccionadas.forEach((item, index) => {
                        if (+item.id === +empresa) {
                            this.empresaSeleccionadas.splice(index, 1);
                        }
                    });

                } else {
                    this.notificaciones.error('No se pudo realizar el proceso ' + response.message);
                }
            }, () => {
                this.notificaciones.error('No se pudo realizar el proceso');
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            })
    }

}