import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';
import { DatosPersonalesRoutingModule } from './datos_personales.routing.module';
import { DatosPersonalesComponent } from './datos_personales.component';
import { DatosPersonalesServices } from './datos_personales.services';
import { EmpresasServices } from '../empresas.module/empresas.services';
import { ProyectosServices } from '../proyectos.module/proyectos.services';



@NgModule({
    declarations: [
        DatosPersonalesComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        DatosPersonalesRoutingModule,
        CustomFormsModule,
        ComponentsModule
    ],
    providers: [
        DatosPersonalesServices,
        UsuariosServices,
        EmpresasServices ,
        ProyectosServices
    ],
})
export class DatosPersonalesModule { } 
