import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatosPersonalesComponent } from './datos_personales.component';

const routes: Routes = [
    {
        path: '',
        component: DatosPersonalesComponent,
        data: {
            pageTitle: 'Datos Personales'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DatosPersonalesRoutingModule {
}
