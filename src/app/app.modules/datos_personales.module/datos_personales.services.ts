import { Injectable } from "@angular/core";
import { HttpServices } from "app/app.services/HttpServices";
import { Observable } from "rxjs/Observable";
import { UsuariosServices } from "../usuarios.module/usuarios.services";


@Injectable()
export class DatosPersonalesServices {

    constructor(private HttpServices: HttpServices,
        private usuariosServices: UsuariosServices) {

    }

    desAsociarProyectos(usuario, data) {
        return this.usuariosServices.desAsociarProyectos(usuario, data);
    }

    desAsociarEmpresas(usuario, data) {
        return this.usuariosServices.desAsociarEmpresas(usuario, data);
    }


}