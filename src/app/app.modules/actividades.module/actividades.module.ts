import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { ActividadesComponent } from './actividades.component'; 
import { ActividadesServices } from './actividades.services';
import { ActividadesRoutingModule } from './actividades.routing.module';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';



@NgModule({
    declarations: [
        ActividadesComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ActividadesRoutingModule,
        CustomFormsModule,
        ComponentsModule
    ],
    providers: [
        ActividadesServices,
        UsuariosServices
    ],
})
export class ActividadesModule { } 
