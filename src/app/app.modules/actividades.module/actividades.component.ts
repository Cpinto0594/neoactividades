import { Component, OnInit } from '@angular/core';
import { NotificationServices } from 'app/app.services/NotificationService';
import { ActividadesServices } from './actividades.services';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { Util } from 'app/app.services/Util.services';
import * as moment from 'moment';
import { AuthService } from 'app/app.services/AuthService';
import { LoadingService } from 'app/app.services/LoadingService';

declare var $: any;

@Component({
    selector: 'actividades-form',
    templateUrl: './actividades.component.html'
})
export class ActividadesComponent implements OnInit {

    public objectActividades: any;
    public arrEmpresas: Array<any>;
    public arrProyectos: Array<any>;
    public arrActividades: Array<any>;

    constructor(private actividadesServices: ActividadesServices,
        private notificaciones: NotificationServices,
        private userServices: UsuariosServices,
        private utilities: Util,
        private authService: AuthService,
        private loadingService: LoadingService) {
    }

    ngOnInit() {
        this.objectActividades = { dia: moment(new Date()).format(this.utilities.DATE_FORMAT) };
        this.arrActividades = [];
        this.findUserData();
        this.getActividades();
    }

    findUserData() {

        this.userServices.getById(this.authService.userLoggedData().userId)
            .subscribe(data => {
                if (data.success) {
                    let _data = data.data;
                    this.arrEmpresas = _data.empresas.map(empresa => { return { id: empresa.empresa_id, descripcion: empresa.descripcion } });
                    this.arrProyectos = _data.proyectos.map(proyecto => { return { id: proyecto.proyecto_id, descripcion: proyecto.descripcion } });
                }
            }, () => { },
                () => {
                });
    }

    getDayLabel() {
        if (!this.objectActividades.dia) return '';
        return this.utilities.getDayOfWeek(new Date(this.objectActividades.dia).getDay() + 1).name;
    }

    getCantidadHoras() {
        this.objectActividades.cantidad_horas = 0;
        if (!this.objectActividades.hora_fin || !this.objectActividades.hora_inicio) return;

        let start = moment('1994-05-09 ' + this.objectActividades.hora_inicio);
        let end = moment('1994-05-09 ' + this.objectActividades.hora_fin);
        var duration = moment.duration(end.diff(start));
        this.objectActividades.cantidad_horas = Math.round(duration.asHours());
    }

    getActividades() {
        let data = {
            usuario: this.authService.userLoggedData().userName,
            desde: moment(this.objectActividades.dia).format(this.utilities.DATE_FORMAT),
            hasta: moment(this.objectActividades.dia).format(this.utilities.DATE_FORMAT)
        };

        this.loadingService.showLoading();
        this.actividadesServices.export(data)
            .subscribe((data) => {
                if (data.success) {
                    this.arrActividades = data.data;
                    this.ordenarActividades();
                } else {
                    this.notificaciones.error('No se pudo cargar información de las actividades');
                }
            },
                () => {
                    this.notificaciones.error('No se pudo obtener información de las actividades');
                    this.loadingService.closeLoading()
                }, () => {
                    this.loadingService.closeLoading();
                });
    }


    ordenarActividades() {
        this.arrActividades.sort((a, b) => {
            return (a.hora_inicio > b.hora_inicio) ? 1 : ((b.hora_inicio > a.hora_inicio) ? -1 : 0);
        });
    }

    guardarActividad() {

        if (!this.objectActividades.dia ||
            !this.objectActividades.hora_inicio ||
            !this.objectActividades.hora_fin ||
            !this.objectActividades.empresa_id ||
            !this.objectActividades.proyecto_id ||
            !this.objectActividades.descripcion ||
            !this.objectActividades.cantidad_horas) {
            this.notificaciones.info("Debe ingresar la información requerida");
            return;
        }


        let dataObject = {
            empresa_id: this.objectActividades.empresa_id.id,
            proyecto_id: this.objectActividades.proyecto_id.id,
            usuario: this.authService.userLoggedData().userName,
            descripcion: this.objectActividades.descripcion,
            dia: this.objectActividades.dia,
            hora_fin: this.objectActividades.hora_fin,
            hora_inicio: this.objectActividades.hora_inicio,
            id: this.objectActividades.id,
            cantidad_horas: this.objectActividades.cantidad_horas,
            estado: this.objectActividades.estado || 'A'
        }

        this.loadingService.showLoading();

        if (dataObject.id) {
            this.actividadesServices.edit(dataObject.id, dataObject)
                .subscribe((response) => {
                    if (response.success) {
                        this.notificaciones.success('Registro exitoso');
                        dataObject['nombreEmpresa'] = this.objectActividades.empresa_id.text;
                        dataObject['nombreProyecto'] = this.objectActividades.proyecto_id.text;
                        this.arrActividades[this.objectActividades.index] = dataObject;
                        this.afterSave();
                    } else {
                        this.notificaciones.error('No se pudo registrar. ' + response.message);
                        this.loadingService.closeLoading()
                    }
                }, () => { this.notificaciones.error('No se pudo registrar'); },
                    () => { this.loadingService.closeLoading() })
        } else {
            this.actividadesServices.save(dataObject)
                .subscribe((response) => {
                    if (response.success) {
                        this.notificaciones.success('Registro exitoso');
                        dataObject.id = response.data.actividadId;
                        dataObject['nombreEmpresa'] = this.objectActividades.empresa_id.text;
                        dataObject['nombreProyecto'] = this.objectActividades.proyecto_id.text;
                        this.arrActividades.push(dataObject);
                        this.afterSave();
                    } else {
                        this.notificaciones.error('No se pudo registrar. ' + response.message);
                        this.loadingService.closeLoading()
                    }
                }, () => { this.notificaciones.error('No se pudo registrar'); this.loadingService.closeLoading() },
                    () => { this.loadingService.closeLoading() })
        }

        $('html, body').scrollTop();

    }

    afterSave() {
        this.objectActividades = { dia: moment(this.objectActividades.dia).format(this.utilities.DATE_FORMAT) };
        $('#empresa').val('').trigger('change');
        $('#proyecto').val('').trigger('change');
        this.ordenarActividades();
        $('html , body').animate({
            scrollTop: $("#seccionActividades").offset().top
        }, 500);
    }

    nuevaActividad() {
        this.afterSave();
        $('html , body').animate({
            scrollTop: $("#seccionActividades").offset().top
        }, 500);
    }

    editarActividad(actividad, index) {
        this.objectActividades = Object.assign({}, actividad);
        this.objectActividades.index = index;
        this.objectActividades.empresa_id = { id: actividad.empresa_id, text: actividad.nombreEmpresa };
        this.objectActividades.proyecto_id = { id: actividad.proyecto_id, text: actividad.nombreProyecto };
        this.objectActividades.dia = moment(this.objectActividades.dia).format(this.utilities.DATE_FORMAT)
        $('#empresa').val(this.objectActividades.empresa_id.id).trigger('change');
        $('#proyecto').val(this.objectActividades.proyecto_id.id).trigger('change');
        $('html , body').animate({
            scrollTop: $("#seccionActividades").offset().top
        }, 500);
    }

    eliminarActividad(actividad) {
        this.loadingService.showLoading();
        this.actividadesServices.delete(actividad)
            .subscribe(response => {
                if (response.success) {
                    this.notificaciones.success('Removido con éxito');
                    this.arrActividades.forEach((item, index) => {
                        if (item.id === actividad)
                            this.arrActividades.splice(index, 1);
                    });
                } else {
                    this.notificaciones.error('No se pudo eliminar actividad');
                }
            },
                () => {
                    this.notificaciones.error('No se pudo eliminar actividad');
                    this.loadingService.closeLoading()
                },
                () => {
                    this.loadingService.closeLoading();
                })
    }
}