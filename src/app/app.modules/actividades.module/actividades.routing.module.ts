import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActividadesComponent } from './actividades.component';

const routes: Routes = [
    {
        path: '',
        component: ActividadesComponent,
        data: {
            pageTitle: 'Listado de Actividades'
        }
    },
    {
        path: 'crear',
        component: ActividadesComponent,
        data: {
            pageTitle: 'Formulario Actividades'
        }
    },
    {
        path: 'edit/:id',
        component: ActividadesComponent,
        data: {
            pageTitle: 'Formulario Actividades'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActividadesRoutingModule {
}
