import { Injectable } from "@angular/core";
import { HttpServices } from "app/app.services/HttpServices";
import { Observable } from "rxjs/Observable";


@Injectable()
export class ActividadesServices {

    private ACTIVIDADES_PATH = '/actividades';

    constructor(private HttpServices: HttpServices) {

    }

    getAllActive(): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/getAllActive');
    }
    getById(id): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/find/' + id);
    }
    save(data): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/', data);
    }
    edit(id, data): Observable<any> {
        return this.HttpServices.put(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/' + id, data);
    }
    delete(id): Observable<any> {
        return this.HttpServices.delete(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/' + id);
    }
    export(object): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.ACTIVIDADES_PATH + '/export', object);
    }

}