import { Component, OnInit } from '@angular/core';
import { ProyectosServices } from './proyectos.services';
import { NotificationServices } from 'app/app.services/NotificationService';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'proyectos-form',
    templateUrl: './proyectos.component.html'
})
export class ProyectosComponent implements OnInit {

    private proyectoId: Number;
    public proyecto: any;

    constructor(private proyectosServices: ProyectosServices,
        private notificaciones: NotificationServices,
        private route: ActivatedRoute,
        private router: Router,
        private loadingService: LoadingService) {
        this.proyectoId = +this.route.snapshot.paramMap.get('id') || 0;
    }

    ngOnInit() {
        this.proyecto = { estado: 'A' };
        if (this.proyectoId) this.findDetalle(this.proyectoId)
    }


    findDetalle(id) {
        this.loadingService.showLoading();

        this.proyectosServices.getById(id)
            .subscribe((data) => {
                this.proyecto = data.data;
            }, () => {
                this.notificaciones.error('No se pudo obtener información');
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            })
    }

    guardarProyecto() {
        if (!this.proyecto || !this.proyecto.codigo || !this.proyecto.descripcion) {
            this.notificaciones.info('Debe Ingresar la información Requerida');
            return;
        }
        this.loadingService.showLoading();

        if (this.proyectoId) {
            this.proyectosServices.edit(this.proyectoId, this.proyecto)
                .subscribe((data) => {
                    if (data.success) {
                        this.notificaciones.success('Actualización Exitosa');
                    } else {
                        this.notificaciones.error('No se pudo registrar información ' + data.message);
                    }
                }, () => {
                    this.notificaciones.error('No se pudo registrar información');
                    this.loadingService.closeLoading()
                }, () => {
                    this.loadingService.closeLoading();
                });
        } else {
            this.proyectosServices.save(this.proyecto)
                .subscribe((data) => {
                    if (data.success) {
                        this.notificaciones.success('Registro Exitoso');
                        this.router.navigate(['proyectos']);
                    } else {
                        this.notificaciones.error('No se pudo registrar información ' + data.message);
                    }
                }, () => {
                    this.notificaciones.error('No se pudo registrar información');
                    this.loadingService.closeLoading()
                }, () => {
                    this.loadingService.closeLoading();
                })
        }
    }

}
