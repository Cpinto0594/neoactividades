import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProyectosListComponent } from './proyectos_list.component';
import { ProyectosComponent } from './proyectos.component';

const routes: Routes = [
    {
        path: '',
        component: ProyectosListComponent,
        data: {
            pageTitle: 'Listado de Proyectos'
        }
    },
    {
        path: 'crear',
        component: ProyectosComponent,
        data: {
            pageTitle: 'Crear Proyectos'
        }
    },
    {
        path: 'edit/:id',
        component: ProyectosComponent,
        data: {
            pageTitle: 'Editar Proyectos'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProyectosRoutingModule {
}
