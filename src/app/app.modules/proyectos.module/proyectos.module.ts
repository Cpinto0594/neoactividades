import { NgModule } from '@angular/core';
import { ProyectosListComponent } from './proyectos_list.component';
import { CommonModule } from '@angular/common';
import { ProyectosRoutingModule } from './proyectos.routing.module';
import { ProyectosServices } from './proyectos.services';
import { ProyectosComponent } from './proyectos.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';



@NgModule({
    declarations: [
        ProyectosListComponent,
        ProyectosComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ProyectosRoutingModule,
        ComponentsModule,
        CustomFormsModule
    ],
    providers: [
        ProyectosServices
    ],
})
export class ProyectosModule { } 
