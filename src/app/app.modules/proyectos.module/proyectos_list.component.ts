import { Component, OnInit } from '@angular/core';
import { ProyectosServices } from './proyectos.services';
import { NotificationServices } from 'app/app.services/NotificationService';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'proyectos-list',
    templateUrl: './proyectos_list.component.html'
})
export class ProyectosListComponent implements OnInit {
    public arrProyectos: Array<any>;


    constructor(private proyectosServices: ProyectosServices,
        private notificaciones: NotificationServices,
        private loadingService: LoadingService) {

    }

    ngOnInit() {
        this.listAllActive();
    }


    listAllActive() {
        this.loadingService.showLoading();

        this.proyectosServices.getAllActive()
            .subscribe((data) => {
                this.arrProyectos = data.data;
                this.loadingService.closeLoading();
            }, (error) => {
                this.notificaciones.error(error.message);
                this.loadingService.closeLoading();
            });
    }

    eliminarProyecto(id) {
        this.loadingService.showLoading();

        this.proyectosServices.delete(id)
            .subscribe((data) => {
                console.log(data)
                if (data.success) {
                    this.arrProyectos.forEach((emp, index) => {
                        if (emp.id === id) {
                            this.arrProyectos.splice(index, 1);
                        }
                    });

                } else {
                    this.notificaciones.error(data.message);
                }
            }, (error) => {
                this.notificaciones.error(error.message);
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            });
    }


}
