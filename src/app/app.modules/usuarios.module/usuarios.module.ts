import { NgModule } from '@angular/core';
import { UsuariosListComponent } from './usuarios_list.component';
import { CommonModule } from '@angular/common';
import { UsuariosRoutingModule } from './usuarios.routing.module';
import { UsuariosServices } from './usuarios.services';
import { UsuariosComponent } from './usuarios.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { EmpresasServices } from '../empresas.module/empresas.services';
import { ProyectosServices } from '../proyectos.module/proyectos.services';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';



@NgModule({
    declarations: [
        UsuariosListComponent,
        UsuariosComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        UsuariosRoutingModule,
        ComponentsModule,
        CustomFormsModule
    ],
    providers: [
        UsuariosServices,
        EmpresasServices,
        ProyectosServices
    ],
})
export class UsuariosModule { } 
