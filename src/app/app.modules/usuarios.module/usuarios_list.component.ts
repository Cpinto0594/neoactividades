import { Component, OnInit } from '@angular/core';
import { UsuariosServices } from './usuarios.services';
import { NotificationServices } from 'app/app.services/NotificationService';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'usuarios-list',
    templateUrl: './usuarios_list.component.html'
})
export class UsuariosListComponent implements OnInit {
    public arrUsuarios: Array<any>;


    constructor(private usuariosServices: UsuariosServices,
        private notificaciones: NotificationServices,
        private loadingService: LoadingService) {

    }

    ngOnInit() {
        this.listAllActive();
    }


    listAllActive() {
        this.loadingService.showLoading();

        this.usuariosServices.getAllActive()
            .subscribe((data) => {
                this.arrUsuarios = data.data;
                this.loadingService.closeLoading();
            }, (error) => {
                this.notificaciones.error(error.message);
                this.loadingService.closeLoading();
            });
    }

    eliminarUsuario(id) {
        this.loadingService.showLoading();

        this.usuariosServices.delete(id)
            .subscribe((data) => {
                this.arrUsuarios.forEach((emp, index) => {
                    if (emp.id === id) {
                        this.arrUsuarios.splice(index, 1);
                    }
                })
                this.loadingService.closeLoading();
            }, (error) => {
                this.notificaciones.error(error.message);
                this.loadingService.closeLoading();
            });
    }


}
