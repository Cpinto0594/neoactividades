import { Component, OnInit } from '@angular/core';
import { NotificationServices } from 'app/app.services/NotificationService';
import { ActivatedRoute, Router } from '@angular/router';

import { EmpresasServices } from '../empresas.module/empresas.services';
import { ProyectosServices } from '../proyectos.module/proyectos.services';
import { UsuariosServices } from './usuarios.services';
import { sha256 } from 'js-sha256';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'usuarios-form',
    templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {

    private usuarioId: Number;
    public usuario: any;
    public empresasList: Array<any>;
    public proyectosList: Array<any>;
    public empresaSeleccionadas: Array<any>;
    public proyectosSeleccionados: Array<any>;
    public empresaSeleccionada: any;
    public proyectoSeleccionada: any;
    public lastClave: string;

    constructor(private usuariosServices: UsuariosServices,
        private notificaciones: NotificationServices,
        private route: ActivatedRoute,
        private router: Router,
        private empresasServices: EmpresasServices,
        private proyectosServices: ProyectosServices,
        private loadingService: LoadingService) {
        this.usuarioId = +this.route.snapshot.paramMap.get('id') || 0;
    }

    ngOnInit() {
        this.usuario = { estado: 'A' };
        this.findEmpresas();
        this.findProyectos();
        this.empresaSeleccionadas = [];
        this.proyectosSeleccionados = [];
        if (this.usuarioId) this.findDetalle(this.usuarioId);
    }


    findDetalle(id) {
        this.loadingService.showLoading('Cargando ...');
        this.usuariosServices.getById(id)
            .subscribe((data) => {
                let _data = data.data;
                this.usuario = _data.usuario;
                this.lastClave = this.usuario.clave;
                this.usuario.clave = null;
                this.empresaSeleccionadas = _data.empresas.map(empresa => 
                    { return { id: empresa.empresa_id, text: empresa.descripcion } });
                this.proyectosSeleccionados = _data.proyectos.map(proyecto => 
                    { return { id: proyecto.proyecto_id, text: proyecto.descripcion } });
                this.loadingService.closeLoading();
            },
                () => {
                    this.notificaciones.error('No se pudo obtener información');
                    this.loadingService.closeLoading();
                })
    }

    findEmpresas() {
        this.empresasServices.getAllActive()
            .subscribe((data) => {
                this.empresasList = (data.data || [])
            })
    }
    findProyectos() {
        this.proyectosServices.getAllActive()
            .subscribe((data) => {
                this.proyectosList = (data.data || [])
            });
    }

    agregarProyecto() {
        if (!this.proyectoSeleccionada) return;
        let exist = this.proyectosSeleccionados
            .map(proyecto => +proyecto.id).indexOf(+this.proyectoSeleccionada.id) !== -1

        if (!exist)
            this.proyectosSeleccionados.push(this.proyectoSeleccionada);
        this.proyectoSeleccionada = null;

    }
    agregarEmpresa() {
        if (!this.empresaSeleccionada) return;
        let exist = this.empresaSeleccionadas
            .map(empresa => +empresa.id).indexOf(+this.empresaSeleccionada.id) !== -1

        if (!exist)
            this.empresaSeleccionadas.push(this.empresaSeleccionada);
        this.empresaSeleccionada = null;
    }

    guardarUsuario() {
        if (!this.usuario || !this.usuario.usuario || !this.usuario.nombre_completo
            || !this.usuario.identificacion || !this.usuario.email  || (!this.usuario.id && !this.usuario.clave)) {
            this.notificaciones.info('Debe Ingresar la información Requerida');
            return;
        }
        this.loadingService.showLoading();
        this.usuario.usuario = this.usuario.usuario.toUpperCase();
        if (!this.usuario.clave) {
            this.usuario.clave = this.lastClave;
        } else {
            this.usuario.clave = sha256(this.usuario.clave);
        }


        let data = {
            usuario: this.usuario,
            empresas: this.empresaSeleccionadas.map(empresa => +empresa.id),
            proyectos: this.proyectosSeleccionados.map(proyecto => +proyecto.id)
        }

        this.usuariosServices.createAndAsocciate(data)
            .subscribe(data => {
                if (data.success) {
                    this.notificaciones.success('Registro Exitoso');
                    this.router.navigate(['usuarios']);
                } else {
                    this.notificaciones.error('No se pudo registrar información ' + data.message);
                }

                this.loadingService.closeLoading();
            },
                () => {
                    this.notificaciones.error('No se pudo registrar información');
                    this.loadingService.closeLoading();
                })
    }

    eliminarproyecto(proyecto) {
        if (!proyecto || !this.usuario || !this.usuario.id) {
            this.notificaciones.error('Debe Seleccionar Datos para la aliminación');
            return;
        }

        this.loadingService.showLoading();

        let data = {
            proyecto: proyecto
        }
        this.usuariosServices.desAsociarProyectos(this.usuario.id, data)
            .subscribe(response => {
                if (response.success) {

                    this.proyectosSeleccionados.forEach((item, index) => {
                        if (+item.id  === +proyecto) {
                            this.proyectosSeleccionados.splice(index, 1);
                        }
                    });

                } else {
                    this.notificaciones.error('No se pudo realizar el proceso ' + response.message);
                }
            }, () => {
                this.notificaciones.error('No se pudo realizar el proceso');
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            })

    }

    eliminarEmpresa(empresa) {
        if (!empresa || !this.usuario || !this.usuario.id) {
            this.notificaciones.error('Debe Seleccionar Datos para la aliminación');
            return;
        }

        this.loadingService.showLoading();

        let data = {
            empresa: empresa
        }
        this.usuariosServices.desAsociarEmpresas(this.usuario.id, data)
            .subscribe(response => {
                if (response.success) {

                    this.empresaSeleccionadas.forEach((item, index) => {
                        if (+item.id === +empresa) {
                            this.empresaSeleccionadas.splice(index, 1);
                        }
                    });

                } else {
                    this.notificaciones.error('No se pudo realizar el proceso ' + response.message);
                }
            }, () => {
                this.notificaciones.error('No se pudo realizar el proceso');
                this.loadingService.closeLoading()
            }, () => {
                this.loadingService.closeLoading();
            })
    }

}
