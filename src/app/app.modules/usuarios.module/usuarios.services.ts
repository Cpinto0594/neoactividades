import { Injectable } from "@angular/core";
import { HttpServices } from "app/app.services/HttpServices";
import { Observable } from "rxjs/Observable";
import { RequestOptions, URLSearchParams } from "@angular/http";


@Injectable()
export class UsuariosServices {

    private USUARIOS_PATH = '/usuarios';
    private LOGIN_PATH = '/access';

    constructor(private HttpServices: HttpServices) {

    }

    getAllActive(): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/getAllActive');
    }
    getById(id): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/find/' + id);
    }
    save(data): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/', data);
    }
    edit(id, data): Observable<any> {
        return this.HttpServices.put(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/' + id, data);
    }
    delete(id): Observable<any> {
        return this.HttpServices.delete(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/' + id);
    }
    createAndAsocciate(data): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/createAndAsocciate', data);
    }
    login(data): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.LOGIN_PATH + '/login', data);
    }
    desAsociarProyectos(usuario, data): Observable<any> {
        let httpOption = new RequestOptions();
        httpOption.params = new URLSearchParams().paramsMap = data;
        return this.HttpServices.put(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/desAsociarProyectos/' + usuario, {}, httpOption);
    }
    desAsociarEmpresas(usuario, data): Observable<any> {
        let httpOption = new RequestOptions();
        httpOption.params = new URLSearchParams().paramsMap = data;

        return this.HttpServices.put(this.HttpServices.getFullApiPath() + this.USUARIOS_PATH + '/desAsociarEmpresas/' + usuario, {}, httpOption);
    }

    checkToken(token): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.LOGIN_PATH + '/check', {});
    }


}