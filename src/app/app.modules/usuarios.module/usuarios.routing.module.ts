import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosListComponent } from './usuarios_list.component';
import { UsuariosComponent } from './usuarios.component';

const routes: Routes = [
    {
        path: '',
        component: UsuariosListComponent,
        data: {
            pageTitle: 'Listado de Usuarios'
        }
    },
    {
        path: 'crear',
        component: UsuariosComponent,
        data: {
            pageTitle: 'Crear Usuarios'
        }
    },
    {
        path: 'edit/:id',
        component: UsuariosComponent,
        data: {
            pageTitle: 'Editar Usuarios'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsuariosRoutingModule {
}
