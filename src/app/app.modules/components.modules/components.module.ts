import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Select2Module } from './select2/select2.module';
import { SmartClockPickerModule } from './clock_picker/smart-clockpicker.module';
import { CustomFormsModule } from './forms.modules/forms_modules.module';
import { LoadingService } from 'app/app.services/LoadingService';



@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        FormsModule,
        Select2Module,
        SmartClockPickerModule,
        CustomFormsModule
    ],
    providers: [
        LoadingService
    ],
    exports: [
        Select2Module,
        SmartClockPickerModule,
        CustomFormsModule
    ]
})
export class ComponentsModule { } 
