import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormButtonsComponent } from './form_buttons.component';
import { FormContentComponent } from './form_content.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormButtonsComponent,
    FormContentComponent
  ],
  exports: [
    FormButtonsComponent,
    FormContentComponent
  ],
})
export class CustomFormsModule { }
