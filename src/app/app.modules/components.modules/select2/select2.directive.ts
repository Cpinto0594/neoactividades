import { Directive, ElementRef, Input, OnInit, Output, EventEmitter } from '@angular/core';

declare var $: any;


@Directive({
    selector: '[select2]'
})
export class Select2Directive implements OnInit {

    private defaultAjaxOptions = {
        url: undefined,
        dataType: 'json',
        type: "GET",
        delay: 250,
        data: function (term) {
            return term;
        },
        processResults: function (data) {
            return {
                results: data
            };
        }

    };

    @Input() select2: any;
    @Output() onSelect: EventEmitter<any> = new EventEmitter<any>();
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter<any>();

    @Input() ajaxOptions: any;

    constructor(private el: ElementRef) {
        $(this.el.nativeElement).addClass(['sa-cloak', 'sa-hidden'].join(' '))
    }

    ngOnInit() {

        this.buildOptions();

        System.import('script-loader!select2/dist/js/select2.min.js').then(() => {
            $(this.el.nativeElement).select2(this.select2);
            $(this.el.nativeElement).removeClass('sa-hidden');
            let super_ = this;
            $(this.el.nativeElement).on('select2:select', function (e) {
                super_.ngModelChange.emit(e.params.data);
                super_.onSelect.emit(e.params.data);
            });

        })
    }

    buildOptions() {
        if (!this.select2) {
            this.select2 = {};
        }

        if (this.ajaxOptions) {
            var ajaxOption = $.extend({}, this.defaultAjaxOptions, this.ajaxOptions);
            $.extend(this.select2, { ajax: ajaxOption });
        }
    }

}
