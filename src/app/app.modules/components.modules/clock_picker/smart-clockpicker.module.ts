import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartClockpickerDirective } from './smart-clockpicker.directive';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SmartClockpickerDirective],
  exports: [SmartClockpickerDirective],
})
export class SmartClockPickerModule { }
