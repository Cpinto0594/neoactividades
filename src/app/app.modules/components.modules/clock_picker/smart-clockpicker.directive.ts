import { Directive, ElementRef, OnInit, Input, Output, EventEmitter } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[smartClockpicker]'
})
export class SmartClockpickerDirective implements OnInit {

  @Input() smartClockpicker: any;
  @Output() onSelect: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDone: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter<any>();

  private _options = {
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    donetext: 'Done',
    icon: 'glyphicon glyphicon-time'
  }

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    System.import('script-loader!clockpicker/dist/bootstrap-clockpicker.min.js').then(() => {
      this.render()
    })
  }


  render() {

    let finalOptions: any = this._options;

    if (this.smartClockpicker) {
      finalOptions = $.extend({}, this._options, this.smartClockpicker);
    }

    var input = $(this.el.nativeElement);
    var spanButton;
    var inputContainer;

    inputContainer = $('<div/>').addClass('input-group clockpicker');
    spanButton = $('<span/>').addClass('input-group-addon');
    spanButton.append($('<span/>').addClass(finalOptions.icon))
    input.wrap(inputContainer);
    spanButton.insertAfter(input);

    let $this = this;

    finalOptions.afterDone = () => {
      this.onDone.emit();
    }
    finalOptions.init = function () {
      input.on('change', function () {
        $this.ngModelChange.emit(this.value);
        $this.onSelect.emit(this.value);
      });

      spanButton.click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show');
      });
    }
    input.clockpicker(finalOptions);
  }

}
