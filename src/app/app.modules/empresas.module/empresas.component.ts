import { Component, OnInit } from '@angular/core';
import { HttpServices } from 'app/app.services/HttpServices';
import { EmpresasServices } from './empresas.services';
import { NotificationServices } from 'app/app.services/NotificationService';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'empresas-form',
    templateUrl: './empresas.component.html'
})
export class EmpresasComponent implements OnInit {

    private empresaId: Number;
    public empresa: any;

    constructor(private empresasServices: EmpresasServices,
        private notificaciones: NotificationServices,
        private route: ActivatedRoute,
        private router: Router,
        private loadingService: LoadingService) {
        this.empresaId = +this.route.snapshot.paramMap.get('id') || 0;
    }

    ngOnInit() {
        this.empresa = { estado: 'A' };
        if (this.empresaId) this.findDetalle(this.empresaId)
    }


    findDetalle(id) {
        this.loadingService.showLoading();

        this.empresasServices.getById(id)
            .subscribe((data) => {
                this.empresa = data.data;
            }, () => {
                this.loadingService.closeLoading();
            },
                () => {
                    this.loadingService.closeLoading();
                })
    }

    guardarEmpresa() {
        if (!this.empresa || !this.empresa.codigo || !this.empresa.descripcion) {
            this.notificaciones.info('Debe Ingresar la información Requerida');
            return;
        }
        this.loadingService.showLoading();

        if (this.empresaId) {
            this.empresasServices.edit(this.empresaId, this.empresa)
                .subscribe((data) => {
                    if (data.success) {
                        this.notificaciones.success('Actualización Exitosa');
                    } else {
                        this.notificaciones.error('No se pudo registrar información ' + data.message);
                    }
                }, () => {
                    this.notificaciones.error('No se pudo registrar información');
                    this.loadingService.closeLoading()
                }, () => {
                    this.loadingService.closeLoading()
                });
        } else {
            this.empresasServices.save(this.empresa)
                .subscribe((data) => {
                    if (data.success) {
                        this.notificaciones.success('Registro Exitoso');
                        this.router.navigate(['empresas']);
                    } else {
                        this.notificaciones.error('No se pudo registrar información ' + data.message);
                    }
                }, () => {
                    this.notificaciones.error('No se pudo registrar información');
                    this.loadingService.closeLoading()
                }, () => {
                    this.loadingService.closeLoading()
                })
        }
    }

}
