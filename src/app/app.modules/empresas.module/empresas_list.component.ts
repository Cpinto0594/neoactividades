import { Component, OnInit } from '@angular/core';
import { HttpServices } from 'app/app.services/HttpServices';
import { EmpresasServices } from './empresas.services';
import { NotificationServices } from 'app/app.services/NotificationService';
import { LoadingService } from 'app/app.services/LoadingService';

@Component({
    selector: 'empresas-list',
    templateUrl: './empresas_list.component.html'
})
export class EmpresasListComponent implements OnInit {
    public arrEmpresas: Array<any>;


    constructor(private empresasServices: EmpresasServices,
        private notificaciones: NotificationServices,
        private loadingServices: LoadingService) {

    }

    ngOnInit() {
        this.listAllActive();
    }


    listAllActive() {
        this.loadingServices.showLoading();

        this.empresasServices.getAllActive()
            .subscribe((data) => {
                this.arrEmpresas = data.data;
            }, (error) => {
                console.log(error)
                this.notificaciones.error(error.message);
            }, () => {
                this.loadingServices.closeLoading();
            });
    }

    eliminarEmpresa(id) {

        this.loadingServices.showLoading();

        this.empresasServices.delete(id)
            .subscribe((data) => {
                this.arrEmpresas.forEach((emp, index) => {
                    if (emp.id === id) {
                        this.arrEmpresas.splice(index, 1);
                    }
                })
            }, (error) => {
                this.notificaciones.error(error.message);
            }, () => {
                this.loadingServices.closeLoading();
            });
    }


}
