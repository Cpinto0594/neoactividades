import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresasListComponent } from './empresas_list.component';
import { EmpresasComponent } from './empresas.component';

const routes: Routes = [
    {
        path: '',
        component: EmpresasListComponent,
        data: {
            pageTitle: 'Listado de Empresas'
        }
    },
    {
        path: 'crear',
        component: EmpresasComponent,
        data: {
            pageTitle: 'Formulario Empresas'
        }
    },
    {
        path: 'edit/:id',
        component: EmpresasComponent,
        data: {
            pageTitle: 'Formulario Empresas'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmpresasRoutingModule {
}
