import { NgModule } from '@angular/core';
import { EmpresasListComponent } from './empresas_list.component';
import { CommonModule } from '@angular/common';
import { EmpresasRoutingModule } from './empresas.routing.module';
import { EmpresasServices } from './empresas.services';
import { EmpresasComponent } from './empresas.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';



@NgModule({
    declarations: [
        EmpresasListComponent,
        EmpresasComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ComponentsModule,
        EmpresasRoutingModule,
        CustomFormsModule
    ],
    providers: [
        EmpresasServices
    ],
})
export class EmpresasModule { } 
