import { Injectable } from "@angular/core";
import { HttpServices } from "app/app.services/HttpServices";
import { Observable } from "rxjs/Observable";


@Injectable()
export class EmpresasServices {

    private EMPRESAS_PATH = '/empresas';

    constructor(private HttpServices: HttpServices) {

    }

    getAllActive(): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.EMPRESAS_PATH + '/getAllActive');
    }
    getById(id): Observable<any> {
        return this.HttpServices.get(this.HttpServices.getFullApiPath() + this.EMPRESAS_PATH + '/find/' + id);
    }
    save(data): Observable<any> {
        return this.HttpServices.post(this.HttpServices.getFullApiPath() + this.EMPRESAS_PATH + '/', data);
    }
    edit(id, data): Observable<any> {
        return this.HttpServices.put(this.HttpServices.getFullApiPath() + this.EMPRESAS_PATH + '/' + id, data);
    }
    delete(id): Observable<any> {
        return this.HttpServices.delete(this.HttpServices.getFullApiPath() + this.EMPRESAS_PATH + '/' + id);
    }

}