import { NgModule } from '@angular/core';
import { UsuariosListComponent } from './usuarios_list.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginServices } from './login.services';
import { LoginRoutingModule } from './login.routing.module';
import { LoginComponent } from './login.component';
import { UsuariosModule } from '../usuarios.module/usuarios.module';
import { UsuariosServices } from '../usuarios.module/usuarios.services';



@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoginRoutingModule
    ],
    providers: [
        LoginServices,
        UsuariosServices
    ],
})
export class LoginModule { } 
