import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
        data: {
            pageTitle: 'Login'
        }
    },
    {
        path: 'logout',
        component: LoginComponent,
        data: {
            pageTitle: 'Cerrar Sesion'
        }
    },
    {
        path: 'change-password',
        component: LoginComponent,
        data: {
            pageTitle: 'Cambiar Clave'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule {
}
