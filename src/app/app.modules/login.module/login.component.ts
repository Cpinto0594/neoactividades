import { Component, OnInit } from '@angular/core';
import { LoginServices } from './login.services';
import { sha256 } from 'js-sha256';
import { NotificationServices } from 'app/app.services/NotificationService';
import { Router } from '@angular/router';
import { AuthService } from 'app/app.services/AuthService';
import { LoadingService } from 'app/app.services/LoadingService';
import { MenuConfig } from 'assets/js/sidebarmenu';
import { CustomAppConfig } from 'assets/js/custom';

@Component({
    selector: 'login-form',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    public userLogin: any;
    public isLogginIn: boolean;
    constructor(private loginServices: LoginServices,
        private notificaciones: NotificationServices,
        private router: Router,
        private authService: AuthService,
        private loadingService: LoadingService
    ) {

    }

    ngOnInit() {
        this.userLogin = {};
    }


    login() {

        if (!this.userLogin.clave || !this.userLogin.usuario) {
            return false;
        }

        if (this.isLogginIn) return;
        this.isLogginIn = true;

        let data = {
            usuario: this.userLogin.usuario.toUpperCase(),
            clave: sha256(this.userLogin.clave),
            loggedFrom:'NA-WEB'
        };
        this.loadingService.showLoading();
        this.loginServices.login(data)
            .subscribe(response => {

                if (response.success) {
                    this.notificaciones.success('Bienvenido ' + this.userLogin.usuario);
                    this.loadingService.closeLoading();
                    this.afterLogin(response.data);
                    return true;
                } else {
                    this.isLogginIn = false;
                    this.notificaciones.error('Credenciales Incorrectas');
                    return false;
                }

            },
                () => {
                    this.notificaciones.error('No se pudo iniciar sesión'); this.isLogginIn = false;
                    this.loadingService.closeLoading();
                },
                () => {
                    this.loadingService.closeLoading();
                    this.isLogginIn = false;
                });


    }

    afterLogin(data) {
        this.authService.loginUser(data);
        this.router.navigate(['actividades']);
        setTimeout(() => {
            MenuConfig.initMenu();
            CustomAppConfig.initComponents();
        }, 500);

    }

}