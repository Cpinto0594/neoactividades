import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { UsuariosServices } from "../usuarios.module/usuarios.services";


@Injectable()
export class LoginServices {


    constructor(private usuarioServices: UsuariosServices) {

    }


    login(data): Observable<any> {
        return this.usuarioServices.login(data);
    }


}