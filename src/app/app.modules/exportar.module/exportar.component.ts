import { Component, OnInit } from '@angular/core';
import { NotificationServices } from 'app/app.services/NotificationService';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { Util } from 'app/app.services/Util.services';
import * as moment from 'moment';
import { AuthService } from 'app/app.services/AuthService';
import { ActividadesServices } from '../actividades.module/actividades.services';
import XlsExport from 'xlsexport';
import { LoadingService } from 'app/app.services/LoadingService';
import { of } from 'rxjs/observable/of';

declare var $: any;

@Component({
    selector: 'exportar-form',
    templateUrl: './exportar.component.html'
})
export class ExportarComponent implements OnInit {

    public exportarObj: any;
    public arrUsuarios: Array<any>;
    public arrDataList: Array<any>;
    private isAdmin: boolean;

    constructor(
        private notificaciones: NotificationServices,
        private userServices: UsuariosServices,
        private utilities: Util,
        private authService: AuthService,
        private actividadesService: ActividadesServices,
        private loadingService: LoadingService) {
        this.exportarObj = {
            desde: moment().format(this.utilities.DATE_FORMAT),
            hasta: moment().format(this.utilities.DATE_FORMAT),
            usuario: ''
        };
        this.arrDataList = [];
        this.isAdmin = this.authService.isAdmin();
    }

    ngOnInit() {
        moment.locale('es');
        this.findUserData();
    }

    findUserData() {
        this.loadingService.showLoading();

        if (this.isAdmin) {
            this.userServices.getAllActive()
                .subscribe(data => {
                    if (data.success) {
                        this.arrUsuarios = data.data;
                    }
                }, () => {
                    this.loadingService.closeLoading()
                    this.notificaciones.error('No se pudo obtener información de Usuarios');
                }, () => {
                    this.loadingService.closeLoading();
                });
        } else {
            of(this.authService.userLoggedData())
                .subscribe(data => {
                    this.arrUsuarios = [{
                        id: data.userId,
                        identificacion: data.userIdentificacion,
                        nombre_completo: data.userFullName,
                        usuario: data.userName
                    }];
                    this.exportarObj.usuario = { id: data.userId, text: data.userFullName };
                }, () => {
                    this.loadingService.closeLoading();
                    this.notificaciones.error('No se pudo obtener información del usuario');
                }, () => {
                    this.loadingService.closeLoading();
                    setTimeout(() => {
                        $('#usuario').val(this.arrUsuarios[0].id).trigger('change');
                        console.log(this.exportarObj.usuario)
                    }, 100)

                });
        }
    }

    labelSemana() {
        if (!this.exportarObj.desde || !this.exportarObj.hasta) return '';

        var start = moment(this.exportarObj.desde);
        var end = moment(this.exportarObj.hasta);
        return 'Actividades desde el ' + start.format('DD [de] MMMM') +
            ' - ' + end.format('DD [de] MMMM') + ' del ' + start.format('YYYY');
    }

    listarDatos() {
        if (!this.exportarObj.usuario.id && !this.isAdmin) {
            return;
        }

        var usuario = this.arrUsuarios.find(user => +user.id === +this.exportarObj.usuario.id) || {};

        let dataExport = {
            usuario: usuario.usuario,
            desde: moment(this.exportarObj.desde).format(this.utilities.DATE_FORMAT),
            hasta: moment(this.exportarObj.hasta).format(this.utilities.DATE_FORMAT)
        };
        this.loadingService.showLoading();

        this.actividadesService.export(dataExport)
            .subscribe(response => {
                if (response.success) {
                    this.arrDataList = response.data;
                } else {
                    this.notificaciones.error('No se pudo extraer datos ' + response.message);
                }
            }, () => { this.notificaciones.error('No se pudo extraer datos '); this.loadingService.closeLoading() },
                () => {
                    this.loadingService.closeLoading();
                });

    }

    exportarDatos() {
        if (!this.arrDataList) {
            this.notificaciones.info('No hay datos a exportar');
            return;
        }

        let dataToExport: Array<any> = [];
        let usuariosToExport: Array<any> = this.arrUsuarios.map(us => us.nombre_completo);
        let emptyUser = {
            usuario: '',
            dia: '',
            hora_inicio: '',
            hora_fin: '',
            empresa: '',
            proyecto: '',
            cantidad_horas: '',
            actividad: ''
        };


        for (var i = 0; i < usuariosToExport.length; i++) {

            let userData = this.arrDataList
                .filter(data => data.nombre_completo === usuariosToExport[i])
                .map(row => {
                    return {
                        usuario: usuariosToExport[i],
                        dia: moment(row.dia).format(this.utilities.DATE_FORMAT),
                        hora_inicio: row.hora_inicio,
                        hora_fin: row.hora_fin,
                        empresa: row.nombreEmpresa,
                        proyecto: row.nombreProyecto,
                        cantidad_horas: row.cantidad_horas,
                        actividad: row.descripcion
                    }
                });


            if (userData && userData.length) {
                dataToExport = dataToExport.concat(
                    userData
                        .concat([emptyUser, emptyUser])
                );
            }

        }

        let xls = new XlsExport(dataToExport);
        xls.exportToXLS('Actividades.xls');
    }


}