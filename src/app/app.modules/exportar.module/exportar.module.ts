import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components.modules/components.module';
import { UsuariosServices } from '../usuarios.module/usuarios.services';
import { CustomFormsModule } from '../components.modules/forms.modules/forms_modules.module';
import { ExportarComponent } from './exportar.component';
import { ExportarRoutingModule } from './exportar.routing.module';
import { ActividadesServices } from '../actividades.module/actividades.services';



@NgModule({
    declarations: [
        ExportarComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ExportarRoutingModule,
        CustomFormsModule,
        ComponentsModule
    ],
    providers: [
        ActividadesServices,
        UsuariosServices
    ],
})
export class ExportarModule { } 
