import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './notfound.component';

const routes: Routes = [
    {
        path: '',
        component: NotFoundComponent,
        data: {
            pageTitle: '404'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NotFoundRoutingModule {
}
