import { Component } from '@angular/core';
import { Menu } from '../app.models/Menu';
import { AuthService } from 'app/app.services/AuthService';
import { DataGlobal } from 'app/app.services/app.dataGlobal';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SideBarComponent {
  public userRoles: string;
  constructor(private authService: AuthService,
    private dataGlobal: DataGlobal) {
    this.userRoles = this.authService.userLoggedData().roles || '';
  }

  public arrMenu: Array<Menu> = this.dataGlobal.arrMenu;


}
