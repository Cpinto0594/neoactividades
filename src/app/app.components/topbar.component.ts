import { Component } from '@angular/core';
import { AuthService } from 'app/app.services/AuthService';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html'
})
export class TopBarComponent {

  public userData: any;

  constructor(private authService: AuthService) {

    this.userData = this.authService.userLoggedData();

  }


  logOut() {
    this.authService.logOut();
  }
}
