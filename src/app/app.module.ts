import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions } from '@angular/http';
import { AppComponent } from './app.component';
import { FooterComponent } from './app.components/footer.component';
import { SideBarComponent } from './app.components/sidebar.component';
import { BreadCrumbComponent } from './app.components/breadcrumb.component';
import { TopBarComponent } from './app.components/topbar.component';
import { RoutingConfig } from './app.routing';
import { DashBoardComponent } from './app.components/dashboard.component';
import { SessionChecker } from './app.services/app.guard.check';
import { AuthService } from './app.services/AuthService';
import { FilterPipe } from './app.pipes/filter.pipe';
import { HttpServices } from './app.services/HttpServices';
import { NotificationServices } from './app.services/NotificationService';
import { Util } from './app.services/Util.services';
import { PushNotificationsService } from './app.services/PushNotificationService';
import { LoadingService } from './app.services/LoadingService';
import { RequestOptionsService } from './app.services/RequestOptions';
import { DataGlobal } from './app.services/app.dataGlobal';
import { MenuAccessChecker } from './app.services/app.menu.access.check';
import { FireBaseConfig } from './app.notifications/firebase_init';
import { LoginAccessChecker } from './app.services/app.session.check.';
import { HTTP_INTERCEPTORS, HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http'; 
import { ValidateTokenInterceptor } from './app.services/ValidateTokenInterceptor';
import { UsuariosServices } from './app.modules/usuarios.module/usuarios.services';
import { UserDashBoardComponent } from './app.modules/dashboard.module/user_dashboard.component';



@NgModule({
  declarations: [
    FilterPipe,
    AppComponent,
    FooterComponent,
    SideBarComponent,
    BreadCrumbComponent,
    TopBarComponent,
    DashBoardComponent,
    UserDashBoardComponent

  ],
  imports: [
    BrowserModule,
    RoutingConfig,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    
    DataGlobal,
    SessionChecker,
    LoginAccessChecker,
    MenuAccessChecker,
    AuthService,
    //{ provide: RequestOptions, useClass: RequestOptionsService },Works only with Http class not HttpClient
    { provide: HTTP_INTERCEPTORS, useClass: ValidateTokenInterceptor, multi: true },
    HttpServices,
    NotificationServices,
    Util,
    PushNotificationsService,
    LoadingService,
    FireBaseConfig,
    UsuariosServices

  ],
  exports: [
  ],

  bootstrap: [AppComponent]
})
export class AppModule { } 
