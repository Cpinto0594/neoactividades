import { Component } from '@angular/core';
import { MenuConfig } from 'assets/js/sidebarmenu';
import { CustomAppConfig } from 'assets/js/custom';
import { Router, NavigationEnd } from '@angular/router';
import { PushNotificationsService } from './app.services/PushNotificationService';
import { FireBaseConfig } from './app.notifications/firebase_init';
import { AuthService } from './app.services/AuthService';
import { UsuariosServices } from './app.modules/usuarios.module/usuarios.services';
import { NotificationServices } from './app.services/NotificationService';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  public fireBaseConfig: any;

  constructor(private router: Router,
    private pushNotifService: PushNotificationsService,
    private firebaseConfiguration: FireBaseConfig,
    private authService: AuthService,
    private usuariosServices: UsuariosServices,
    private notificationServices: NotificationServices) {

    //Detenemos el preloader
    //Configuramos el menú
    console.log('App.compontnt')
    this.router.events.filter(event => event instanceof NavigationEnd)
      .distinctUntilChanged()
      .subscribe(change => {
        $(".preloader").fadeOut();
      });

    setTimeout(() => {
      MenuConfig.initMenu();
      CustomAppConfig.initComponents();
      this.pushNotifService.requestPermission();
      this.checkToken();
      //Inicialize firebase

      //this.firebaseConfiguration.configure();

      // let data: Array<any> = [];
      // data.push({
      //   'title': 'Approval',
      //   'alertContent': 'This is First Alert -- By Debasis Saha'
      // });
      // this.pushNotifService.generateNotification(data);



    }, 500)

  }
  checkToken() {
    let userLogged = this.authService.userLoggedData();
    if (!userLogged)
      return;

    this.usuariosServices.checkToken(null)
      .subscribe(
        (resp) => {
          if (!resp.success) {
            
          }
        },
        () => { })
  }



}