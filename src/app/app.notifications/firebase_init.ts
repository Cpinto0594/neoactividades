import { Injectable } from "@angular/core";
import { GLOBALS } from "app/Globals";

declare var firebase: any;

@Injectable()
export class FireBaseConfig {

    public messagingServer: any;

    configure() {
        this.initConfig()
            .then(() => {
                this.requestPermission()
                    .then(() => {
                        this.registerServiceWorker()
                            .then(() => {
                                this.requestTokenListener()
                                    .then(() => {
                                    });
                            })
                    })
            });


    }

    initConfig() {
        return new Promise((succ, fail) => {

            try {

                // Initialize Firebase
                if (firebase && firebase.initializeApp instanceof Function) {
                    console.log('Inicializando Firebase', firebase);
                    firebase.initializeApp(GLOBALS.firebaseConfig);
                    this.messagingServer = firebase.messaging();
                    this.messagingServer.usePublicVapidKey('BKHZzGuabQt-LoWkga3jPzYAcyNg3iJ96OiW_9Tdye52_6Uroav2gSyeU2qHOGTSF6sbGDHgz7D-I1SR6FCwVQI');
                    succ(this.messagingServer);
                }
            } catch (e) {
                console.log('Could not configure Firebase');
                fail(e);
            }
        });

    }


    requestPermission() {
        return this.messagingServer.requestPermission()
            .then(() => {
                console.log('Notification permission granted.');

            }).catch(function (err) {
                console.log('Unable to get permission to notify.', err);
            });
    }

    registerServiceWorker() {
        return navigator.serviceWorker.register('assets/js/firebase-messaging-sw.js')
            .then(
                (registration) => {
                    this.messagingServer.useServiceWorker(registration);
                },
                (fail) => {
                    console.log('No se pudo iniciar el Service Worker ' + fail);
                });
    }

    requestTokenListener() {
        return new Promise((succ, fail) => {

            try {
                this.messagingServer.onTokenRefresh(() => {
                    succ();
                }, (err) => {
                    console.log('Unable to load token Listener ', err);
                    //showToken('Unable to retrieve refreshed token ', err);
                    fail(err)
                });
            } catch (e) {
                fail(e)
            }

        })

    }

    requestToken() {
        return this.messagingServer.getToken()
            .then(function (refreshedToken) {
                console.log('Token refreshed.', refreshedToken);
                // Indicate that the new Instance ID token has not yet been sent to the
                // app server.
                //setTokenSentToServer(false);
                // Send Instance ID token to app server.
                //sendTokenToServer(refreshedToken);
                // ...
            }).catch(function (err) {
                console.log('Unable to retrieve refreshed token ', err);
                //showToken('Unable to retrieve refreshed token ', err);
            });
    }


}