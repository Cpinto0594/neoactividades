import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { AuthService } from "./AuthService";
import { Menu } from "app/app.models/Menu";
import { DataGlobal } from "./app.dataGlobal";

@Injectable()
export class MenuAccessChecker implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router,
        private dataGlobal: DataGlobal
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): boolean {
        let rolesUsuario = this.authService.userLoggedData().roles;
        if (!rolesUsuario) {
            return false;
        }
        let routeToSctivate = state.url;
        let menu: Menu = (this.dataGlobal.arrMenu).filter((menu: Menu) => routeToSctivate === menu.menuLink)[0];
        let canActivate =  menu.canShow(rolesUsuario);
        return canActivate;
    }


}