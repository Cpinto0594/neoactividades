import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { AuthService } from "./AuthService";

@Injectable()
export class LoginAccessChecker implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router
    ) {

    }

    //EN este caso validaremos si se puede acceder a la vista de login
    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): boolean {
        let loggedIn = this.authService.isLoggedUser();
        if (loggedIn) {
            return false;
        }

        return true;
    }


}