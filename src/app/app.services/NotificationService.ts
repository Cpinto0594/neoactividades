import { Injectable } from "@angular/core";

declare var mkNotifications: any;
declare var mkNoti: any;

@Injectable()
export class NotificationServices {
    private options =
        {
            icon: {
                color: '#FFFFFF'
            },
            sound: true,
            duration: 1500
        };

    constructor() {
        var config =
        {
            positionY: "top"
        };

        mkNotifications(config);
    }

    info(message, callback?: Function, options?: any) {
        message = (message || '').substring(0, 130);
        let _options = Object.assign({}, this.options, { status: "info", duration: 1800 }, options, { callback: callback })
        mkNoti(
            "Información",
            message,
            _options
        );
    }

    error(message, callback?: Function, options?: any) {
        message = (message || '').substring(0, 130);
        let _options = Object.assign({}, this.options, { status: "danger", duration: 2500 }, options, { callback: callback })

        mkNoti(
            "Error",
            message,
            _options
        );
    }

    success(message, callback?: Function, options?: any) {
        message = (message || '').substring(0, 130);
        let _options = Object.assign({}, this.options, { status: "success" }, options, { callback: callback })

        mkNoti(
            "Correcto",
            message,
            _options
        );
    }

}