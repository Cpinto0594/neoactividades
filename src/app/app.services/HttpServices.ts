import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, RequestOptions, Headers } from "@angular/http";

import { GLOBALS } from "app/Globals";
import 'rxjs/Rx';
import { AuthService } from "./AuthService";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class HttpServices {

    constructor(private http: HttpClient,
        private authService: AuthService) {

    }

    setHeadersToken(options: RequestOptions) {
        options = options || new RequestOptions;
        if (!options.headers) {
            options.headers = new Headers;
        }
        
        return options;
    }

    get(url: string, options?: RequestOptions): Observable<any> {
        options = this.setHeadersToken(options);
        let headers: any = options.headers;
        return this.http.get(url, { headers: new HttpHeaders(headers) });
    }

    post(url: string, data: any, options?: RequestOptions): Observable<any> {
        options = this.setHeadersToken(options);
        let headers: any = options.headers;
        return this.http.post(url, data, { headers: new HttpHeaders(headers) });
    }

    put(url: string, data: any, options?: RequestOptions): Observable<any> {
        options = this.setHeadersToken(options);
        let headers: any = options.headers;
        return this.http.put(url, data, { headers: new HttpHeaders(headers) });
    }

    delete(url: string, options?: RequestOptions): Observable<any> {
        options = this.setHeadersToken(options);
        let headers: any = options.headers;
        return this.http.delete(url, { headers: new HttpHeaders(headers) });
    }

    getFullApiPath() {
        return GLOBALS.SERVER_IP + GLOBALS.API_PATH;
    }


}