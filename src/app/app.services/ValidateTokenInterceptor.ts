import {
    HttpEvent, HttpInterceptor, HttpHandler,
    HttpRequest, HttpErrorResponse
} from "@angular/common/http";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { AuthService } from "./AuthService";
import { GLOBALS } from "app/Globals";
import { _throw as throwError, _throw } from 'rxjs/observable/throw';
import { NotificationServices } from "./NotificationService";

@Injectable()
export class ValidateTokenInterceptor implements HttpInterceptor {

    constructor(
        private authService: AuthService,
        private notificacionesService: NotificationServices
    ) {
        console.log('Interceptor');
    }

    intercept(request: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {


        request = request.clone(
            {
                headers: request.headers
                    .set('appKey', GLOBALS.API_KEY)
                    .set('X-Forwarded-For', window.location.origin)
            });

        if (this.authService.isLoggedUser()) {
            request = request.clone({
                headers: request.headers
                    .set('Authorization', 'Bearer ' + this.authService.userLoggedData().userToken)
                    .set('X-RFTK', this.authService.userLoggedData().RfTkn)
            })
        }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {

                return event;
            }),
            catchError((error) => {
                console.log(error)
                if (error && ['INV_TOKEN', 'NO_TOKEN'].indexOf(error.errorType)) {
                    this.notificacionesService.error('Token de acceso no se puede validar', () => {
                        this.authService.logOut();
                    });
                }

                return _throw(error);
            })
        );
    }
}