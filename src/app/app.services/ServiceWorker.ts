import { Injectable } from "@angular/core";

@Injectable()
export class ServiceWorker {

    checkAvailability() {
        return ('serviceWorker' in navigator);
    }

    registerServiceWorker = async (file) => {
        const swRegistration = await navigator.serviceWorker.register(file); //notice the file name
        console.log(swRegistration);
        return swRegistration;
    }

}