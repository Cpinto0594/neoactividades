import { Injectable } from "@angular/core";
import { UserLoggedModel } from "../app.models/UserLoggedModel";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";

@Injectable()
export class AuthService {

    constructor(private router: Router) {

    }

    isLoggedUser(): boolean {
        return this.userLoggedData() != null && this.userLoggedData().userToken != null;
    }

    isAdmin() {
        return this.isLoggedUser() && this.userLoggedData().roles.indexOf('ADM') !== -1;
    }

    userLoggedData(): UserLoggedModel {
        let userData: any = localStorage.getItem('usrDta');
        if (!userData) return null;
        userData = JSON.parse(userData);
        return userData;
    }

    loginUser(usuario: any): Observable<UserLoggedModel> {


        let observer = new Observable<UserLoggedModel>(subscriber => subscriber.next(null));

        let localStorage = window.localStorage;
        if (!localStorage) {
            console.log('No se puede persistir la sesion, pero puede continuar');
            return observer;
        }   

        let userData: UserLoggedModel = usuario;
        AuthService.setUserData(userData);


        return observer;
    }

    public static setUserData(userData) {
        localStorage.removeItem("usrDta");
        localStorage.setItem("usrDta", JSON.stringify(userData));

    }

    logOut() {
        localStorage.removeItem("usrDta");
        this.router.navigate(['login']);
    }


}