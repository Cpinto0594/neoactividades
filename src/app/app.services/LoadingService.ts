import { Injectable } from "@angular/core";

declare var $: any;

@Injectable()
export class LoadingService {

    showLoading(text?: string) {

        $.Toast.showToast({
            // toast message
            "title": text || 'Procesando información...',
            duration:null
        });


    }


    closeLoading() {
        $.Toast.hideToast();
    }

}