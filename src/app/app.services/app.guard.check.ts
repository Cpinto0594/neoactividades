import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { AuthService } from "./AuthService";

@Injectable()
export class SessionChecker implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): boolean {
        let loggedIn = this.authService.isLoggedUser();
        if (loggedIn) {
            return true;
        }

        this.router.navigate(['login']);
        return false;
    }


}