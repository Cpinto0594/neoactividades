import { Injectable } from "@angular/core";
import { BaseRequestOptions } from "@angular/http";
import { GLOBALS } from "app/Globals";

@Injectable()
export class RequestOptionsService extends BaseRequestOptions {
    constructor() {
        super();
        console.log('Setting headers')
        this.headers.set('appKey', GLOBALS.API_KEY);
        this.headers.set('X-Forwarded-For' , window.location.origin);
    }
}