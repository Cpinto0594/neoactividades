import { Injectable } from "@angular/core";
import { Menu } from "app/app.models/Menu";

@Injectable()
export class DataGlobal {
    public arrMenu: Array<Menu> = [
        new Menu(1, "Generales", "GEN", null, 'mdi mdi-receipt', 'ADM,USR', 0, true),
        new Menu(2, "Actividades", "ACT", '/actividades', 'mdi mdi-responsive', 'ADM,USR', 0),
        new Menu(3, "Exportar", "EXP", '/exportar', 'mdi mdi-view-dashboard', 'ADM,USR'),
        new Menu(4, "Empresas", "GEN_EMP", '/empresas', 'mdi mdi-city', 'ADM,USR', 1),
        new Menu(5, "Proyectos", "GEN_PRO", '/proyectos', 'mdi mdi-trello', 'ADM,USR', 1),
        new Menu(6, "Usuarios", "GEN_USU", '/usuarios', 'mdi mdi-account-plus', 'ADM', 1),
        new Menu(7, "Datos Personales", "GEN_HOJAV", '/datos-personales', 'mdi mdi-account-plus', 'ADM,USR', 1)
    ];

}