import { Injectable } from "@angular/core";

@Injectable()
export class Util {
    public DATE_FORMAT = 'YYYY-MM-DD';
    private arrDayNames = [
        { dayOfWeek: 1, name: 'Lunes' },
        { dayOfWeek: 2, name: 'Martes' },
        { dayOfWeek: 3, name: 'Miercoles' },
        { dayOfWeek: 4, name: 'Jueves' },
        { dayOfWeek: 5, name: 'Viernes' },
        { dayOfWeek: 6, name: 'Sábado' },
        { dayOfWeek: 7, name: 'Domingo' },
    ]


    getDayOfWeek(_day) {
        return this.arrDayNames.filter(day => day['dayOfWeek'] === +_day)[0];
    }

}