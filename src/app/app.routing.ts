import { RouterModule, Routes } from '@angular/router';
import { DashBoardComponent } from "./app.components/dashboard.component";
import { ModuleWithProviders } from '@angular/core';
import { SessionChecker } from './app.services/app.guard.check';
import { MenuAccessChecker } from './app.services/app.menu.access.check';
import { LoginAccessChecker } from './app.services/app.session.check.';
import { UserDashBoardComponent } from './app.modules/dashboard.module/user_dashboard.component';

export const routes: Routes = [
    {
        path: '',
        component: DashBoardComponent,
        canActivate: [
            SessionChecker
        ],
        data: { pageTitle: 'Home' },
        children: [
            {
                path: '',
                component: UserDashBoardComponent, 
            },
            {
                path: 'empresas',
                loadChildren: 'app/app.modules/empresas.module/empresas.module#EmpresasModule',
                canActivate: [MenuAccessChecker]
            },
            {
                path: 'proyectos',
                loadChildren: 'app/app.modules/proyectos.module/proyectos.module#ProyectosModule',
                canActivate: [MenuAccessChecker]
            },
            {
                path: 'usuarios',
                loadChildren: 'app/app.modules/usuarios.module/usuarios.module#UsuariosModule',
                canActivate: [MenuAccessChecker]
            },
            {
                path: 'actividades',
                loadChildren: 'app/app.modules/actividades.module/actividades.module#ActividadesModule',
                canActivate: [MenuAccessChecker]
            },
            {
                path: 'exportar',
                loadChildren: 'app/app.modules/exportar.module/exportar.module#ExportarModule',
                canActivate: [MenuAccessChecker]
            },
            {
                path: 'datos-personales',
                loadChildren: 'app/app.modules/datos_personales.module/datos_personales.module#DatosPersonalesModule',
                canActivate: [MenuAccessChecker]
            }

        ]
    },
    {
        path: 'login',
        loadChildren: 'app/app.modules/login.module/login.module#LoginModule',
        canActivate:[LoginAccessChecker]
    },
    {
        path: '404',
        loadChildren: 'app/app.modules/404.module/notfound.module#NotFoundModule'
    },
    {
        path: '**',
        redirectTo: '404'
    }
];

export const RoutingConfig: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
